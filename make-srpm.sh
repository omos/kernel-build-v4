#!/bin/bash

set -ex

name="$1"; shift
ranges="$1"; shift
rpm_base="$1"; shift
rpm_target="$1"; shift
debug="$1"; shift
pwurls="$1"; shift

KERNEL_REPO_URL="https://gitlab.com/omos/linux-public.git"
KERNEL_REPO_PATH="cache/linux-public"

RPM_REPO_URL="https://src.fedoraproject.org/rpms/kernel.git"
RPM_REPO_PATH="cache/kernel"

TEST_PATCH_NAME='linux-kernel-test.patch'

SED_SCRIPT_CONFIG='s/^\(\.\/process_configs\.sh\) \$OPTS/\1/g'
SED_SCRIPT_DEBUGBUILDS="s/%define debugbuildsenabled [0-9][0-9]*/%define debugbuildsenabled $(( ! $debug ))/g"

function cd-kernel() { env -C "$KERNEL_REPO_PATH" "$@"; }
function cd-rpm() { env -C "$RPM_REPO_PATH" "$@"; }

if ! [ -d "$KERNEL_REPO_PATH" ]; then
	git clone -n "$KERNEL_REPO_URL" "$KERNEL_REPO_PATH"
	cd-kernel git reset HEAD '*' &>/dev/null
	cd-kernel git checkout --detach &>/dev/null
	cd-kernel git branch -D master &>/dev/null
fi
cd-kernel git fetch origin

[ -d "$RPM_REPO_PATH" ] || git clone -n "$RPM_REPO_URL" "$RPM_REPO_PATH"
cd-rpm git fetch origin
cd-rpm git checkout "$rpm_base"

: >"$RPM_REPO_PATH/$TEST_PATCH_NAME"

for range in $ranges; do
	cd-kernel git format-patch --stdout "$range" -- >>"$RPM_REPO_PATH/$TEST_PATCH_NAME"
done

for pwurl in $pwurls; do
	curl "$pwurl" >>"$RPM_REPO_PATH/$TEST_PATCH_NAME"
done

cat \
	<(echo "%global buildid .$name") \
	<([ $debug -eq 0 ] && echo "%define _with_baseonly 1") \
	<(echo "%define _without_debuginfo 1") \
	<(sed   -e "$SED_SCRIPT_CONFIG" \
		-e "$SED_SCRIPT_DEBUGBUILDS" \
		"$RPM_REPO_PATH/kernel.spec") \
	>"$RPM_REPO_PATH/kernel.spec.new"

cd-rpm mv kernel.spec.new kernel.spec
cd-rpm fedpkg --release "$rpm_target" srpm
cd-rpm git checkout '*'

mv "$RPM_REPO_PATH"/*.src.rpm .
cp "$RPM_REPO_PATH/$TEST_PATCH_NAME" .
