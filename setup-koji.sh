#!/bin/bash

set -ex

cat >/etc/krb5.conf.d/fedoraproject_org <<EOF
[realms]
 FEDORAPROJECT.ORG = {
        kdc = https://id.fedoraproject.org/KdcProxy
 }
[domain_realm]
 .fedoraproject.org = FEDORAPROJECT.ORG
 fedoraproject.org = FEDORAPROJECT.ORG
EOF

sed -i '/KEYRING:persistent/d' /etc/krb5.conf

kinit "$KRB_USERNAME@FEDORAPROJECT.ORG" <<EOF
$KRB_PASSWORD
EOF
