#!/bin/bash

set -ex

rpm_target="$1"; shift
srpm="$1"; shift

task_id="$(fedpkg --release "$rpm_target" scratch-build \
	--arches x86_64 \
	--nowait --fail-fast --srpm "$srpm" | \
	grep 'Created task:' | cut -d ' ' -f 3)"

[ -n "$task_id" ]

echo -n "$task_id" >koji_task_id
