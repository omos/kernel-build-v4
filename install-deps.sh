#!/bin/bash

set -ex

# Update DNF cache from runner cache
if [ -d dnf-cache ]; then
	rm -rf /var/cache/dnf
	mv dnf-cache /var/cache/dnf
fi

DNF_COMMON_OPTS="--setopt=install_weak_deps=False --allowerasing -y"

dnf $DNF_COMMON_OPTS --downloadonly install "$@"

cp -r /var/cache/dnf dnf-cache

dnf $DNF_COMMON_OPTS -C install "$@"
