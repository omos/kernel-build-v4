#!/bin/bash

set -ex

task_id="$(cat koji_task_id)"

koji watch-task "$task_id"

koji download-task --noprogress --logs "$task_id"

ls -l
